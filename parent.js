const child_process = require('child_process');

/**
 * Spawn a new child process. This process will run on a separate thread,
 * allowing the timeout event to properly be caught on the main thread's event loop
 * and fire the callback.
 */
const child = child_process.fork('fake-task', []);

child.on('message', (m) => {
  switch (m.cmd) {
    case 'loopProgress':
      console.log(`Received message from child process: ${m.message}`);
      break;
    case 'loopComplete':
      child.kill();
      break;
  }

})

/**
 * Timeout callback function set to fire in 3 seconds.
 * Callback function should fire in 3 seconds since
 * we are spawning a child process to handle the other task.
 */
const timeoutStart = Date.now();
setTimeout(function() {
  const timeoutComplete = Date.now();
  const durationToFireCallbackSec = (timeoutComplete - timeoutStart) / 1000;
  console.log(`Timeout fired | ${durationToFireCallbackSec} seconds`);
  
  child.send({ cmd: 'parent', message: 'Timeout in parent complete' });
}, 3000);
console.log('Timeout set.');