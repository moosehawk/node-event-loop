const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
const processLength = 500000000;
const startTime = Date.now();

/**
 * Perform the tasks equal to the number of CPUs. Number of CPUs
 * is a good benchmark against the process-multi-threaded.js script.
 */
for (var i = 0; i < numCPUs; i++) {
  performProcess(i);
}

const endTime = Date.now();
const totalTimeSec = (endTime - startTime)/1000;
console.log(`Total processing time: ${totalTimeSec} seconds`);

/**
 * Simulate performing a task that takes some amount of time.
 */
function performProcess(i) {
  const loopStartTime = new Date(Date.now());
  let n = 0;

  console.log(`Started process: ${i} | ${loopStartTime}`);

  /**
   * Handle a "complex" task that will block the CPU from
   * performing other tasks. Insert some complex task here.
   */
  while (n < processLength) {
    n++;

    /**
     * Output a status update on some interval. Can't use
     * setInterval outside of while since the while loop will block
     * execution of events waiting in the event queue.
     */
    if (n % 10000000 == 0) {
      let progress = Math.round(n / processLength * 100, 2);
      console.log(`process update (${i}) | (${progress}%)`);
    }
  }

  let loopEndTime = Date.now();
  let loopTimeSec = (loopEndTime - loopStartTime)/1000;

  console.log(`loop on process (${i}) took ${loopTimeSec} seconds`);
}
