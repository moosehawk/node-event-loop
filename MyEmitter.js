var EventEmitter = require("events");

var MyEmitter = {
  beforeAfter() {
    console.log("before");
    this.emit("fire");
    console.log("after");
  },
  create() {
    var self = Object.create(this);
    Object.assign(self, EventEmitter.prototype);
    EventEmitter.call(self);
    return self;
  }
};

module.exports = MyEmitter;