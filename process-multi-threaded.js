const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
const processLength = 500000000;
const startTime = Date.now();

if (cluster.isMaster) {
  let processesRemaining = numCPUs;

  /**
   * Perform the tasks equal to the number of CPUs. Number of CPUs
   * is a good benchmark against the process-single-threaded.js script.
   */
  for (var i = 0; i < numCPUs; i++) {
    let worker = cluster.fork();

    /**
     * Handle some task when the worker process sends a message back to
     * the master cluster worker.
     */
    worker.on('message', (m) => {
      switch (m.cmd) {
        case 'processUpdate':
          let progress = Math.round(m.n / processLength * 100, 2);
          console.log(`Worker process update (${worker.process.pid}) | (${progress}%)`);
          break;
        case 'processComplete':
          processesRemaining--;

          if (processesRemaining == 0) {
            const endTime = Date.now();
            const totalTimeSec = (endTime - startTime) / 1000;
            console.log(`Total processing time: ${totalTimeSec} seconds`);
          }
          break;
        default:
          console.log(`Received unhandled message: ${m.cmd}`);
      }
    });
  }
} else {
  performProcess();

  /**
   * All code in performProcess() is run synchronously, so kill
   * the cluster worker process once its done.
   */
  cluster.worker.kill();
}

/**
 * Simulate performing a task that takes some amount of time.
 */
function performProcess() {
  const loopStartTime = new Date(Date.now());
  let n = 0;

  console.log(`Started worker process: ${cluster.worker.process.pid} | ${loopStartTime}`);

  /**
   * Handle a "complex" task that will block the CPU from
   * performing other tasks. Insert some complex task here.
   */
  while (n < processLength) {
    n++;

    /**
     * Output a status update on some interval. Can't use
     * setInterval outside of while since the while loop will block
     * execution of events waiting in the event queue.
     * Using process.send() just as an example of sending a message
     * back to the cluster worker master, but this is optional.
     */
    if (n % 10000000 == 0) {
      process.send({ cmd: 'processUpdate', n: n });
    }
  }

  let loopEndTime = Date.now();
  let loopTimeSec = (loopEndTime - loopStartTime) / 1000;

  console.log(`loop on process (${cluster.worker.process.pid}) took ${loopTimeSec} seconds`);

  /**
   * Let the master cluster worker know that the process is complete.
   */
  process.send({ cmd: 'processComplete' });
}
