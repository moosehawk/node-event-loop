/**
 * Handle messages sent to this process
 */
process.on('message', (m) => {
  if (m.cmd == 'parent') {
    console.log(`Received message from parent: ${m.message}`);
  }
});

console.log('Child process started');

let n = 0;
const loopMax = 3000000000;
while (n < loopMax) {
  n++;
  
  if (n % 100000000 == 0) {
    let progress = Math.round(n/loopMax * 100);
    process.send({ cmd: 'loopProgress', message: `Progress on child process loop (${progress}%)` });
  }
}

setImmediate(() => {
  process.send({ cmd: 'loopComplete' });    
});
