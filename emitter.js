var MyEmitter = require("./MyEmitter");

/**
 * Exercise 1: EventEmitter basics
 */
var me = MyEmitter.create();
var me2 = MyEmitter.create();

me.on("fire", function() {
  console.log("emit fired 1");
});

me.on("fire", function() {
  console.log("emit fired 2");
});

me2.on("fire", function() {
  console.log("emit2 fired");
})

me.beforeAfter();
//me2.beforeAfter();

/**
 * Exercise 2: Attempting to emit events before finished instantiating
 */
me.emit("firePre");

me.on("firePre", function() {
  console.log("emit pre fired");  // will not be fired
});

/**
 * Exercise 3: Pre-emitting but changing fire order via event loop setImmediate
 */
setImmediate(function(ee) {
  ee.emit("fireThing");
}, me);

me.on("fireThing", function() {
  console.log("thing fired via setImmediate");
});