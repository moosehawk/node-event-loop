/**
 * Timeout callback function set to fire in 3 seconds.
 * Callback function will not fire in 3 seconds though, since the callback
 * event can't be executed until Node.js is finished executing other tasks.
 */
const timeoutStart = Date.now();
setTimeout(function() {
  const timeoutComplete = Date.now();
  const durationToFireCallbackSec = (timeoutComplete - timeoutStart) / 1000;
  console.log(`Timeout fired | ${durationToFireCallbackSec} seconds`);
}, 3000);
console.log('Timeout set.');

/**
 * This task will halt execution of other events on the event loop
 * until it is finished.
 */
let n = 0;
const loopMax = 3000000000;
while (n < loopMax) {
  n++;
  
  if (n % 100000000 == 0) {
    let progress = Math.round(n/loopMax * 100);
    console.log(`Progress on while loop (${progress}%)`)
  }
}