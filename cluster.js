const cluster = require("cluster");
const http = require("http");
const numCPUs = require("os").cpus().length;

const rssWarn = (18 * 1024 * 1024);

if (cluster.isMaster) {
  console.log(`Master cluster process started.`);

  /**
   * Create number of workers equal to number of CPUs on master
   */
  for (var i = 0; i < numCPUs; i++) {
    createWorker();
  }

  /**
   * Every 1 second, check health of all worker processes
   * to make sure they've responded once in last 5 seconds
   **/
  setInterval(() => {
    var time = new Date().getTime();

    Object.keys(cluster.workers).forEach((id) => {
      if (cluster.workers[id].lastCb + 5000 < time) {
        console.log(`Long running worker ${cluster.workers[id].process.pid} will be killed.`);
        cluster.workers[id].process.kill();
      }
    });
  }, 1000);
} else {
  console.log(`Started process: ${cluster.worker.process.pid}`);

  /**
   * Create worker server.
   * 1 in 20 requests will create an endless loop that will halt all code execution
   * on the worker process. Test this by spamming requests to the server
   * and watch the processes that are being accessed (and halted) in the console output.
   */
  http.Server((req, res) => {
    if (Math.floor(Math.random() * 20) === 4) {
      console.log(`Process ${cluster.worker.process.pid} will be halted in a moment`);
      while(true) { continue }
    }

    res.writeHead(200);
    res.end("hello world\n");
  }).listen(8000);

  /**
   * Every 1 second, send a status message to the worker.
   */
  setInterval(() => {
    process.send({ cmd: "reportMem", memory: process.memoryUsage() });
  }, 1000);
}

/**
 * Create a new worker process.
 * Handles message updates for health reporting, and will re-create itself on exit.
 */
function createWorker() {
  var worker = cluster.fork();
  console.log(`Create worker ${worker.process.pid}`);

  // Set last time worker sent message
  cluster.workers[worker.id].lastCb = new Date().getTime() - 1000;

  worker.on("message", (m) => {
    if (m.cmd === "reportMem") {
      cluster.workers[worker.id].lastCb = new Date().getTime();
      
      if (m.memory.rss > rssWarn) {
        console.log(`worker ${worker.process.pid} using too much memory (${m.memory.rss}).`);
      }
    }
  });

  worker.on("exit", (code, signal) => {
    console.log(`worker ${worker.process.pid} died. Signal: ${signal}. Code: ${code}`);
    createWorker();
  });
}